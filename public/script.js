(function() { /*Скобки здесь важны. Из за них
    выражение превращается в
    Function Expression.
    В результате в глобальный
    объект переменные функции не
    попадут*/

    var HttpProtocol = function() {
        this.get = function(url, next) {
            var XML = new XMLHttpRequest();
            XML.onreadystatechange = function() {
                if (XML.readyState == XMLHttpRequest.DONE && XML.status == 200)
                    next(XML.responseText); // returns the text received from a server following a request being sent
                    // Next simply allows the next route handler in line to handle the request
                };

            XML.open("GET", url, true);  // Определяет метод, URL и другие опциональные параметры запроса;
            // параметр async определяет, происходит ли работа в асинхронном режиме.
            // Последние два параметра необязательны.
            XML.send(null); // Отправляет запрос на сервер.
        };
    };

    var client = new HttpProtocol();

    client.get("/test", function(res) {
        var data = JSON.parse(res).data; // читает объекты из строки в формате JSON
        var test_div = document.querySelector("div"); // Возвращает первый элемент внутри документа (используется предупорядоченный обход узлов в глубину до первого найденного узла), который совпадает с определенной группой селекторов.
        var form_block = document.createElement("form");
        form_block.setAttribute("method", "post");
        form_block.setAttribute("action", "/confirmTest");

        test_div.appendChild(form_block);

        for (var i = 0; i < data.length; i++) {
            var ul = document.createElement("ul");
            var h3 = document.createElement("h3");

            form_block.appendChild(ul);
            h3.innerText = data[i].question;
            ul.appendChild(h3);

            var current_answer = data[i].answers;

            for (var j = 0; j < current_answer.length; j++) {
                var text = data[i].answers[j].answer;
                var inp = document.createElement("input");

                inp.setAttribute("type", "radio");
                inp.setAttribute("name", data[i].question);
                inp.setAttribute("id", "question-" + i + "" + j);
                inp.setAttribute("value", text);
                inp.setAttribute("required", "required");

                var li = document.createElement("li");
                ul.appendChild(li);

                var label = document.createElement("label");
                label.setAttribute("for", "question-" + i + "" + j);
                label.innerText = text;

                li.appendChild(inp);
                li.appendChild(label);
            }
        }

        var submitButton = document.createElement("input");
        submitButton.setAttribute("type", "submit");
        submitButton.setAttribute("value", "Завершить тест");

        form_block.appendChild(submitButton);

        var historyButton = document.createElement("a");
        historyButton.setAttribute("href", "/history");
        historyButton.innerText = "Показать историю";

        form_block.appendChild(historyButton);
    });
})();
