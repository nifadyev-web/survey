# Электронный тесты
## Задача
Разработать веб-приложение, обладающее следующим функционалом: пользователю при открытии страницы приложения предлагается ответить на ряд вопросов. Каждый вопрос представляет собой cледующую структуру:

```JSON
{
	"question": "Вопрос, который необходимо задать",
	"answers":[
	{
		"answer":"вариант ответа №1",
		"correct":true
	},
	{
		"answer":"вариант ответа №2",
		"correct":true
	},
	{
		"answer":"вариант ответа №3",
		"correct":false
	}
	]
}
```
Где количество вариантов ответа на вопрос может быть произвольным.
Вопросы на странице приложения отображаются в виде нумерованного списка, каждый вариант ответа – в виде вложенного ненумерованного списка, содержащего вариант ответа и элемент управления, позволяющий пометить данный ответ как правильный.
По нажатию кнопки «завершить ответ», введенные пользователем ответы сохраняются в базе данных и пользователю отображается информация о прохождении теста в виде таблицы, содержащей два столбца. В первом столбце отображаются номера вопросов, во втором столбце информация о том, правильно пользователь ответил на это вопрос или нет. Тест считается пройденным, если пользователь правильно указал ВСЕ правильные варианты ответа и НЕ указал ни одного неправильного.
Массив вопросов читается из базы данных или из специального каталога на сервере. Пользователь может просмотреть информацию о пройденных ранее тестах.

### Дополнительно
добавить возможность авторизации пользователя и просмотра им только своих вариантов ответов.
