var express = require("express");
var MongoClient = require("mongodb").MongoClient;
var bodyParser = require("body-parser");
var path = require("path");
var fs = require("fs");
var db;

var path_ = path.join(__dirname, ".", "public"); // формируем путь до папки с файлами клиента
var charset = "utf8";

var app = express(); // Init express
var port = process.env.PORT || 3000; // whatever is in the environment variable PORT, or 3000 if there's nothing there

app.use(bodyParser.urlencoded({ extended: true /* false */ })); // Автоматический разбор тела (body) и запроса
// для основных методов HTTP
app.use(bodyParser.json()); // body-parser извлекает всю часть тела входящего потока запросов и предоставляет его на req.body как что-то более легкое для взаимодействия.
app.use("/", express.static(__dirname + "/public")); // работа со статическим содержимым

app.get("/", function(req, res) {
    res.render("index.html");
});
app.get("/test", function(req, res) {
    var filePath = path_ + "/test.json";
    if (fs.existsSync(filePath)) {
        fs.readFile(filePath, charset, function(err, data) {
            var test = JSON.parse(data);
            return res.json({ status: "200", message: "OK", data: test });
        });
    } else {
        return res.json({ status: "404", message: "ERROR", data: null });
    }
});

app.listen(port, function() {
    console.log("localhost");
}); // Связь с портом и запуск сервера

app.post("/confirmTest", function(req, res) {
    var json_template = req.body;
    var keys = Object.keys(json_template); // Object.keys(obj) - возвращает массив – список свойств
    // объекта (только enumerable-свойства)
    var objects = [];

    var db_data = {
        correct_count: 0,
        question_count: Object.keys(json_template).length,
        result: null
    };
    for (var i = 0; i < db_data.question_count; i++) {
        objects[i] = {
            question: keys[i],
            answer: json_template[keys[i]],
            correct: false
        };
    }

    db_data.result = objects;

    var filePath = path_ + "/test.json";
    fs.readFile(filePath, { encoding: charset }, function(err, data) {
        var correct = new Array();
        if (!err) {
            var row = JSON.parse(data);
            for (var i = 0; i < row.length; i++) {
                var current_correct = new Array();
                var keys_length = Object.keys(row[i].answers).length;

                for (var j = 0; j < keys_length; j++) {
                    if (row[i].answers[j].correct == true) {
                        current_correct.push(row[i].answers[j].answer);
                    }
                }
                correct.push(current_correct);
            }

            for (var i = 0; i < db_data.question_count; i++) {
                var size = correct[i].length;
                for (var j = 0; j < size; j++) {
                    if (correct[i][j] == db_data.result[i].answer) {
                        db_data.result[i].correct = true;
                        db_data.correct_count++;
                        break;
                    }
                }
            }

            // Добавление результатов теста в историю
            db.collection("history").insertOne(db_data, function(err, result) {
                if (err) {
                    console.log(err);
                    return res.sendStatus(500);
                }
                var c = db_data.correct_count;
                var q = db_data.question_count;
                // Формирование HTML кода для новой страницы с результатами теста
                var html =
                    `<!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width,initial-scale=1">
                    <title>lab1</title>
                    <link rel="stylesheet" href="style.css">
                </head>
                <body>
                    <h1>Насколько хорошо Вы знаете С++?</h1>	
                    <main>

                    <h3 class="result">Спасибо за прохождение теста! Ваши результаты внесены в базу</h3>

                    <h2 class="` +
                    (c == q ? `success` : `bad`) +
                    `">` +
                    (c == q
                        ? `Тест пройден`
                        : `Тест не пройден. Вы ошиблись ` + (q - c) + ` раза`) +
                    `</h2>
                    
                    <table style="width:100%">
                <tbody>
                  <tr>
                    <th>Вопрос</th>
                    <th>Результат</th> 
                  </tr>`;

                for (var i = 0; i < db_data.result.length; i++) {
                    html += `<tr><td>` + db_data.result[i].question + `</td>`;
                    html +=
                        `<td class="` +
                        (db_data.result[i].correct ? `success` : `bad`) +
                        `">` +
                        (db_data.result[i].correct ? `Верно` : `Неверно`) +
                        `</td></tr>`;
                }

                html += `</tr></tbody></table><a href="/history">Посмотреть историю</a><a href="/">Вернуться на главную</a></main></body></html>`;

                res.send(html);
            });
        } else {
            console.log(err);
        }
    });
});

app.get("/history", function(req, res) {
    db.collection("history")
        .find()
        .toArray(function(err, docs) {
            if (err) {
                console.log(err);
                return res.sendStatus(500);
            }
            res.send(docs);
        });
});

MongoClient.connect(
    "mongodb://localhost:27017/",
    { useNewUrlParser: true },
    function(err, database) {
        if (err) {
            return console.log(err);
        }

        db = database.db("db_name");
        // дропаем базу с каждым перезапуском сервера
        var exists = db.collection("history").findOne();
        if (exists) db.collection("history").drop();
    }
);
